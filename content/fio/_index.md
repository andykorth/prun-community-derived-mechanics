---
title: "FIO"
---

## Using CSV Endpoints In Google Sheets

All CSV endpoints supported can be found [here](https://doc.fnar.net/#/csv).

### Using Public Data Endpoints

Public data is data that isn't user data.  To use these, simply fill cell `A1` on its own sheet (bottom-left tabs are "Sheets") with the following:

```
=IMPORTDATA("https://rest.fnar.net/csv/prices")
```

If you are using a different locale (using a different number separator and such), you must also add `;",";"en_US"` to the IMPORTDATA call. Effectively, what this does is say "the separator is comma and the locale of the data is en_US".  Doing this allows Google properly convert cells to numbers. An example using /csv/prices:

```
=IMPORTDATA("https://rest.fnar.net/csv/prices";",";"en_US")
```

### Using Private Data Endpoints

The use of private data endpoints requires you have a FIO account.  Once your data is in FIO:

- Go to https://fio.fnar.net/settings
- Under **API Keys** click **CREATE API KEY**
   - For Application, type `GoogleSheets`
   - For Password, type your FIO password
- Use the private data endpoints in this manner:

```
=IMPORTDATA("https://rest.fnar.net/csv/workforce?username=YourUserName&apikey=YourAPIKeyGoesHere")
```

You can also use the private data endpoints with group ids as follows:

```
=IMPORTDATA("https://rest.fnar.net/csv/workforce?group=GroupIDGoesHere&apikey=YourAPIKeyGoesHere")
```

## Using FIO Groups

Groups are simply a mechanism of grouping users together. Groups do **not** imply that all members of the group share their data with each other. That being said, in order for groups to function as expected, all users in the group need to share their data via the `Permissions` section of FIO settings.

- Go to https://fio.fnar.net/settings
- Under **Groups** click the **CREATE GROUP** button
- Name the Group and select the users
- Ensure that all members of the group give each other read permissions to their data (you can add the group as having permissions).

Once this is done, select pages on FIO allow you to select a group. You can also use the group id for CSV private data endpoints.

## Using FIDO Group Commands

### Setup

**Each user in the group must do the following:**
- In https://fio.fnar.net/settings
   - Give user `FIDO` read permission do your data.
   - Add the group in question to the permissions list and grant it full permissions
   - Click **SET DISCORD ID** and set it appropriately

Once that is complete, it is ready for use.  To test, use the command:
```
$findmat RAT <GroupId>
```
where `<GroupID>` is the groupid of the group.

If you are the owner (or administrator) of a discord server, you may set the default group id on a per-channel basis. Doing so means you can exclude the GroupId when typing any group commands.  You can accomplish this by typing the following in a channel:
```
$setdefaultgroupid <GroupID>
```

### FIDO Cache

In order to reduce server load and make results return faster, FIDO maintains a **per-user** cache of results for 10 minutes.  What this means is that the first command may be slow in returning the result, but subsequent ones will be faster. This also means that data **may be out of date** if something has changed immediately after the command was issued.  To force clear your user cache, issue the following command:
```
$clearcache
```

### Commands
The following group commands are available for use with FIDO:

Each of these commands require different inputs.  Type `$help <Command>` to see the help description.

- findbui | findbuilding | findbuildings
- findburn
- findconsumer | findconsume | findconsumers
- findmat | findmats | findmaterial
- findprod | findprods | findproducer | findproducers
- findrep | findrepair | findrepairs
- setdefaultgroupid
- clearcache
