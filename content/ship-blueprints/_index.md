---
title: "Ship Blueprints"
---
## Structure Points:

Structural element count `= volume / 21`. This is the number of `SSC` materials needed for the ship.

## Volume of Components:

The volume of each component seems to be consistent regardless of the other blueprint choices on the ship:

Cargo bay volume, relative to the Tiny cargo bay:

| Cargo Bay | Volume (m3) |
| ----------- | ----------- |
| T | 0 |
| VS | 158 |
| S | 420 |
| M | 945 |
| L | 1995 |
| HL | 945 |
| HV | 3045 |

## Mass of Components
Mass of components is not consistent- it changes based on your other design choices. 

How much mass is added for each cargo bay, depending on your fuel tank size?
| Cargo Bay | With two small tanks (m3) | With two Med tanks |
| ----------- | ----------- |----------- |
| T | 0 | 0 |
| VS | 83 | 72 |
| S | 213 | 203 |
| M | 448 | 428 |
| L | 883 | 873 |
| HL | 548 | ? |
| HV | 1199 | 1179 |

Other oddities regarding mass of tanks vs. cargo size:

|tank size |Tiny|	Very Small|	Small cargo|	Med Cargo|	Large Cargo|	HV|	HL|
| ----------- | ----------- |----------- |----------- |----------- |----------- |----------- |----------- |
|STL Fuel Med|	76|	76|	76|	66|	66|	56|	|
|FTL Fuel Med|	16|	15|	16|	16|	26|	16|	|
|STL Fuel Large|	410	| | | | | | |			
|FTL Fuel Large|	52	| | | | | | |

Note that as the cargo bay gets larger, the STL tank gets less massive, but the FTL tank adds mass.


## Component sizes:
Do the sizes of indivual ship components adjust the mass and volume of the ship by the same amoutn when installed? Evidence points to no, component mass/vol is irrelevant to ship size.			

## Build time:
`Build time = Operating Empty Mass / 50`

## Emitter count:

The source code lists some numbers, but there's no obvious way to apply them correctly.
|   | FTL_FIELD_EMITTER_SMALL | FTL_FIELD_EMITTER_MED | FTL_FIELD_EMITTER_LARGE |
| ----------- | ----------- |----------- |----------- |
| FTL_VOLUME_SPAN | 250 | 500 | 1000 |
| POWER_REQUIREMENT | 100 | 175 | 300 |

The dev blog suggests:
>For a ship to be able to perform FTL jumps in our PrUniverse, it has to surround itself with an FTL field. This field is created from a number of emitters placed on the ship. Each emitter can span a certain volume. Now, for the first iteration of ship building, ships will just have to have a set number of emitters that together span at least the ship's total volume.
 (source https://prosperousuniverse.com/blog/2020/07/20/warp-drive-time-244/ )

But the emitter requirement doesn't simply add up to the Span numbers. They also don't line up if you assume they are radii or diameters of spheres. 
